**Documentation**


*RTFM and LMGTFY*

>I use it like 30 minutes ago... The guy I'm working with was trying to run a gateway I wrote for him to test in a client host, but there was a problem with the raw query. He told me that the result set was not correct. He spend like 20 minutes explaining me the problem. It took me like 2 minutes to find the answer, we just needed to add a super simple subquery and tada.


**OS and languages**

>I'm used to using ubuntu o some lightweight linux distribution when using a docker container.

>Right now I'm using windows with WSL2 to run ubuntu at the same time in my personal laptop.

>My main programming language is Python. I've been working with it for at least 4 years.