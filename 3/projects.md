***Personal projects***

>I'm currently working on my most ambitious project "Koto" that stands for King of the oasis. It will be a social network with post linked to a point on the map, which I'll call Oasis. People will be able to like the best posts within each Oasis and every week there will be a King of the Oasis in that place.

**The stack that will use will be:**

    Server: AWS
    Back-end: Django
    Front-end: Unity

>Currently I'm working on the backend using clean architecture.

![](koto.png)

Here is one function "create user" and how I'm using the CA layers. I hope to finish this project in 3 months. 


**Some cool code.**

> If you want to see some cool code I wrote some years ago you can visit: https://github.com/AnnTony1794/python_helpers/blob/main/docorate_specific_functions.py

> I love to create utility and reusable code.
> I came up with this code after thinking about how the pytest module works.