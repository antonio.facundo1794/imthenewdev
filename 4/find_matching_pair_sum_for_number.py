import sys
from bisect import bisect_left


def binary_search(number_list, target, steps):

    low = 0
    high = len(number_list) - 1
    mid = 0
 
    while low <= high:
        steps += 1
        mid = (high + low) // 2
 
        if number_list[mid] < target:
            low = mid + 1
 
        elif number_list[mid] > target:
            high = mid - 1
        else:
            return True, steps
        
    return False, steps



def main(number_list, target):
    steps = 0

    if target > sum(number_list[-2:]):
        return False, (), steps
    
    if target <= number_list[0]:
        return False, (), steps

    while number_list:
        steps += 1

        cut = bisect_left(number_list, target)
        if cut != 0:
             number_list = number_list[:cut]

        first = number_list.pop(-1)
        second = target - first
        result, steps = binary_search(number_list, second, steps)
        if result:
            return True, (first, second), steps
    return False, (), steps

if __name__ == "__main__":
    number_list_str = sys.argv[1]
    number_list = number_list_str.split(",")
    number_list = list(map(lambda x: int(x), number_list))
    target = int(sys.argv[2])

    ok, result, steps = main(number_list=number_list, target=target)

    if ok:
        print(f"Ok, matching pair {result}")
    else:
        print(f"No")

    print(f"Steps: {steps}")