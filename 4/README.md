***Run the docker container***

1.- Build the docker image.
> docker build -t new-dev .

2.- Run the container
> docker run new-dev *number_list *target

Example:
> docker run new-dev "3,4,5,6,8,10,15,17,18" "3"


![](results.png)